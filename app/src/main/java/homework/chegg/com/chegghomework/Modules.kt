package homework.chegg.com.chegghomework

import com.google.gson.annotations.SerializedName

data class NewsFeed(val title: String, val subTitle: String, val image: String)

// data source A related modules
data class DataSourceAResponse(val stories: List<Story>)
data class Story(val imageUrl: String?, val subtitle: String?, val title: String?)


// data source B related modules
data class DataSourceBResponse(val metadata: Metadata)

data class Metadata(
    @SerializedName("this")
    val _this: String,
    val innerdata: List<Innerdata>
)
data class Innerdata(val aticleId: Int,val articlewrapper: Articlewrapper,val picture: String)
data class Articlewrapper(val description: String, val header: String)


// data source C related modules
class SourceCResponse : ArrayList<SourceCResponseItem>()
data class SourceCResponseItem(
    val image: String?,
    val subLine1: String?,
    val subline2: String?,
    val topLine: String?
)