package homework.chegg.com.chegghomework

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import homework.chegg.com.chegghomework.databinding.CardItemBinding

class ArticlesAdapter : ListAdapter<NewsFeed, ArticlesAdapter.NewsFeedHolder>(CALLBACK) {

    companion object {
        @SuppressLint("DiffUtilEquals")
        private val CALLBACK = object : DiffUtil.ItemCallback<NewsFeed>() {
            override fun areItemsTheSame(oldItem: NewsFeed, newItem: NewsFeed) =
                oldItem == newItem

            override fun areContentsTheSame(oldItem: NewsFeed, newItem: NewsFeed) =
                oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsFeedHolder {
        val binding = CardItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NewsFeedHolder(binding)
    }

    override fun onBindViewHolder(holder: NewsFeedHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class NewsFeedHolder(private val binding: CardItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(newsFeed: NewsFeed){
            with(newsFeed){
                binding.textViewCardItemTitle.text = title
                binding.textViewCardItemSubtitle.text = subTitle
                Glide.with(binding.root.context).load(image).into(binding.imageViewCardItem)
            }
        }
    }
}